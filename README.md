# Recipe NoteBook App

List Screen |  Permission Screen |  Permission Screen 
:-------------------------:|:-------------------------:|:-------------------------:
![](https://raw.githubusercontent.com/fikretkoray/placeMarkApp/master/images/listScreen.png)  |  ![](https://raw.githubusercontent.com/fikretkoray/placeMarkApp/master/images/permissionScreen2.png)|  ![](https://raw.githubusercontent.com/fikretkoray/placeMarkApp/master/images/permissionScreen.png)
Current Location Screen |  Save Screen |  Detail Screen
![](https://raw.githubusercontent.com/fikretkoray/placeMarkApp/master/images/currentLocationScreen.png)|  ![](https://raw.githubusercontent.com/fikretkoray/placeMarkApp/master/images/saveScreen.png)|  ![](https://raw.githubusercontent.com/fikretkoray/placeMarkApp/master/images/detailScreen.png)
